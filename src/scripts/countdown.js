import aion from './aion';

class Countdown {
  constructor() {
    this.start = Date.now();
    this.end = Date.parse('12/07/2019');
    this.timer = null;
    this.time = {};
    this.timeDom = {
      d0: 0,
      d1: 0,
      h0: 0,
      h1: 0,
      m0: 0,
      m1: 0,
      s0: 0,
      s1: 0,
    };
    this.init();
  }

  init() {
    this.counter();
    this.timer = setInterval(() => this.counter(), 1000);
  }

  counter() {
    this.start = Date.now();

    const count = this.end - this.start;
    this.time = {
      count,
      days: Math.floor(count / (1000 * 60 * 60 * 24)).toString(),
      hours: Math.floor((count / (1000 * 60 * 60)) % 24).toString(),
      minutes: Math.floor((count / 1000 / 60) % 60).toString(),
      seconds: Math.floor((count / 1000) % 60).toString(),
    };

    this.timeDom.d0 = this.time.days.length > 1 ? this.time.days[0] : 0;
    this.timeDom.d1 = this.time.days.length > 1 ? this.time.days[1] : this.time.days[0];
    this.timeDom.h0 = this.time.hours.length > 1 ? this.time.hours[0] : 0;
    this.timeDom.h1 = this.time.hours.length > 1 ? this.time.hours[1] : this.time.hours[0];
    this.timeDom.m0 = this.time.minutes.length > 1 ? this.time.minutes[0] : 0;
    this.timeDom.m1 = this.time.minutes.length > 1 ? this.time.minutes[1] : this.time.minutes[0];
    this.timeDom.s0 = this.time.seconds.length > 1 ? this.time.seconds[0] : 0;
    this.timeDom.s1 = this.time.seconds.length > 1 ? this.time.seconds[1] : this.time.seconds[0];
  }
}

export default Countdown;