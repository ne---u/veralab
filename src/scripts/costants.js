import { detect } from './detect';

const isSafari = () => {
  const ua = navigator.userAgent.toLowerCase();
  let isSafari = false;
  isSafari = ua.match(/(ipod|iphone|ipad)/) && ua.match(/applewebkit/);
  isSafari = (
    isSafari ||
    ((ua.indexOf('safari') !== -1) &&
    (!(ua.indexOf('chrome') !== -1) &&
    (ua.indexOf('version/') !== -1)))
  );
  return isSafari;
};

export const data = {
  viewport: {
    min: 0,
    max: -11600,
  },
  velocity: 0.0008,
  breakpoint: 5500,
  claim: {
    min: 0,
    max: 0,
  },
  posters: {
    img: [800, 1200],
    video: [2600, 3200],
  },
  bloom: {
    min: 4400,
    max: 4600,
  },
  welcome: {
    min: 1000,
    opacity: 1400,
    max: 2000,
  },
  flagship: {
    min: 3000,
    opacity: 3400,
    max: 3900,
  },
  services: {
    min: [5700, 6700],
    opacity: [5700, 6300],
    scale: [6150, 6650],
    translate: [5900, 6300],
    opacity2: [7650, 7850],
    max: [6700, 7800],
  },
  recycle: {
    min: [7800, 8800],
    opacity: [7800, 8400],
    scale: [8200, 8700],
    translate: [8000, 8500],
    opacity2: [9400, 9800],
    max: [9000, 9800],
  },
  contact: {
    min: 9800,
    opacity: [9800, 10600],
    cta: [10600, 10800],
  },
  cookie: 300,
  touch: !window.matchMedia('(hover: hover)').matches,
  safari: isSafari(),
  detect: {
    tier: detect,
    level: 100,
  },
};

export default data;
