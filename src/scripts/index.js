import anime from 'animejs';
import aion from './aion';
import stage from './stage';
import * as scroll from './scroll';
import { modulate } from './utils';
import Cookie from './cookie';
import data from './costants';
import updateCheckPerf from './detect';
import { Scene1, Scene2, Scene3 } from '../scenes';

import "../styles/index.scss";

export const status = { init: false };

const center = {
  x: window.innerWidth / 2,
  y: window.innerHeight / 2,
};

const mouse = {
  x: center.x,
  y: center.y,
  hover: false,
  ray: {
    x: 0,
    y: 0,
  },
};

export const open = {
  signal: false,
  name: null,
  scale: 0,
  position: {
    x: 0,
    y: 0,
  },
};

let intersect = null;
let intersectSignal = false;
let cookieScroll = false;

const dom = {
  claim: document.querySelector('.claim'),
  welcome: document.querySelector('.welcome'),
  welcomeWrapper: document.querySelector('.welcome__wrapper'),
  flagship: document.querySelector('.flagship'),
  flagshipWrapper: document.querySelector('.flagship__wrapper'),
  services: document.querySelector('.services'),
  recycle: document.querySelector('.recycle'),
  contact: document.querySelector('.contact'),
  ctas: document.querySelector('.contact .ctas'),
  scroll: document.querySelector('.scroll'),
  scrollWrapper: document.querySelector('.scroll__wrapper'),
  scrollAnim: document.querySelector('.scroll__anim'),
  scrollPaths: Array.from(document.querySelectorAll('.scroll__anim path')),
  mask: document.querySelector('.mask'),
  trail: document.querySelector('.trail-mouse'),
  progress: document.querySelector('.progress'),
};

const refresh = () => {
  const isTouch = data.touch;
  if (isTouch !== !window.matchMedia('(hover: hover)').matches) {
    window.location.reload();
  }
};

window.addEventListener('resize', () => { refresh(); });

const video = document.createElement( 'video' );
video.src = '/public/recycle-3.mp4';
video.classList.add('video-recycle');
video.muted = true;
video.loop = true;
video.autoplay = true;
video.load();

const cookie = new Cookie(document.querySelector('.cookie'));
let loader = [];
loader = [
  ...stage.manager('gltf', ['/public/vera_door.glb', '/public/final03.glb', '/public/luce02.glb']),
  ...stage.manager('texture', ['/public/texture2.png', '/public/poster-1.jpg', '/public/poster-2.jpg', '/public/street.jpg', '/public/matcap.png', '/public/olioDenso.png', '/public/luceLiquida.png', '/public/spumone.png', '/public/siero.png', '/public/lucel.png', '/public/recycle-mask.jpg']),
];

aion.start();

aion.add(() => { updateCheckPerf(); }, 'check-perfo', true);

// stage.setDebug();

Promise.all(loader).then((res) => {
  const resources = {
    door: res[0],
    wall: res[1],
    product: res[2],

    floor: res[3],
    poster1: res[4],
    poster2: res[5],
    world: res[6],
    matcap: res[7],
    olio: res[8],
    luce: res[9],
    spumone: res[10],
    box: res[11],
    luce2: res[12],
    recycle: video,
    recycleMask: res[13],
  };

  status.init = true;

  transition();

  cookie.init();

  stage.group('scene1', [0, 0, -1]);
  Scene1.door(resources.door, resources.matcap);
  Scene1.floor(resources.floor);
  Scene1.posters([resources.poster1, resources.poster2]);

  stage.group('scene2', [0, window.innerWidth > 768 ? -0.23 : -0.17, -4.74]);
  Scene2.wall(resources.wall, resources.matcap, [resources.olio, resources.luce, resources.spumone, resources.box]);

  stage.group('scene3', [0, -0.36, -5]);
  Scene3.product(resources.product, resources.matcap, resources.luce2);
  Scene3.recycle([resources.recycle, resources.recycleMask]);
  Scene3.sphere(resources.world);

  aion.add(stage.rendererHandler, 'renderer');

  aion.add(scrollfn, 'SCROLL');

  aion.add(rayFn, 'ray', true);

  if (!data.touch) {
    aion.add(trailFn, 'trail');
  }

  video.play();
});

const scrollfn = () => {
  if (Math.abs(scroll.lerp.y) > data.cookie && !cookieScroll) {
    cookieScroll = true;
    cookie.onAcceptClick();
  }

  const axis = window.innerWidth > 768 ? 'Y' : 'X';
  const scale = modulate(Math.abs(scroll.lerp.y), [data.viewport.min, Math.abs(data.viewport.max)], [0, 1], true);
  dom.progress.style.transform = `scale${axis}(${scale})`;

  if (Math.abs(scroll.lerp.y) < data.breakpoint) {
    stage.camera.position.z = scroll.lerp.y * data.velocity;
    stage.camera.position.y = ((mouse.y / center.y) - 1) * -0.0025;
    stage.camera.position.x = ((mouse.x / center.x) - 1) * -0.0025;
  } else {
    stage.camera.position.z = -data.breakpoint * data.velocity;
    stage.camera.position.y = ((scroll.lerp.y + data.breakpoint) * data.velocity) + (((mouse.y / center.y) - 1) * -0.0025);
  }

  if (Math.abs(scroll.lerp.y) <= data.breakpoint) {
    dom.claim.style.transform = `translate3d(0px, 0px, ${-scroll.lerp.y}px)`;
    dom.scroll.style.transform = `translate3d(0px, 0px, ${-scroll.lerp.y}px)`;
  }

  if (data.safari) {
    if (Math.abs(scroll.lerp.y) >= data.welcome.min && Math.abs(scroll.lerp.y) < data.welcome.opacity) {
      dom.welcomeWrapper.style.opacity = modulate(Math.abs(scroll.lerp.y), [data.welcome.min, data.welcome.opacity], [0, 1], true);
    } else if (Math.abs(scroll.lerp.y) >= data.welcome.opacity && Math.abs(scroll.lerp.y) < data.welcome.max + 200) {
      dom.welcomeWrapper.style.opacity = modulate(Math.abs(scroll.lerp.y), [data.welcome.max, data.welcome.max + 200], [1, 0], true);
    } else {
      dom.welcome.style.transform = `translate3d(0px, 0px, 0px)`;
      dom.welcomeWrapper.style.opacity = 0;
    }

    if (Math.abs(scroll.lerp.y) >= data.flagship.min && Math.abs(scroll.lerp.y) < data.flagship.opacity) {
      dom.flagshipWrapper.style.opacity = modulate(Math.abs(scroll.lerp.y), [data.flagship.min, data.flagship.opacity], [0, 1], true);
    } else if (Math.abs(scroll.lerp.y) >= data.flagship.opacity && Math.abs(scroll.lerp.y) < data.flagship.max + 200) {
      dom.flagshipWrapper.style.opacity = modulate(Math.abs(scroll.lerp.y), [data.flagship.max, data.flagship.max + 200], [1, 0], true);
    } else {
      dom.flagship.style.transform = `translate3d(0px, 0px, 0px)`;
      dom.flagshipWrapper.style.opacity = 0;
    }
  } else {
    dom.welcomeWrapper.style.opacity = modulate(Math.abs(scroll.lerp.y), [data.welcome.min, data.welcome.opacity], [0, 1], true);
    if (Math.abs(scroll.lerp.y) >= data.welcome.min) {
      dom.welcome.style.transform = `translateZ(${(-(scroll.lerp.y + data.welcome.max))}px)`;
    } else {
      dom.welcome.style.transform = `translateZ(0px)`;
    }

    dom.flagshipWrapper.style.opacity = modulate(Math.abs(scroll.lerp.y), [data.flagship.min, data.flagship.opacity], [0, 1], true);
    if (Math.abs(scroll.lerp.y) >= data.flagship.min) {
      dom.flagship.style.transform = `translateZ(${(-(scroll.lerp.y + data.flagship.max))}px)`;
    } else {
      dom.flagship.style.transform = `translateZ(0px)`;
    }
  }
  
  if (Math.abs(scroll.lerp.y) >= data.services.min[0] && Math.abs(scroll.lerp.y) < data.services.min[1]) {
    dom.services.style.opacity = modulate(Math.abs(scroll.lerp.y), data.services.opacity, [0, 1], true);
    dom.services.children[0].style.transform = `translateY(${modulate(Math.abs(scroll.lerp.y), data.services.translate, [100, 0], true)}px)`;
    dom.services.classList.remove('not-visible');
  } else if (Math.abs(scroll.lerp.y) >= data.services.max[0] && Math.abs(scroll.lerp.y) < data.services.max[1]) {
    dom.services.classList.remove('not-visible');
    dom.services.style.opacity = modulate(Math.abs(scroll.lerp.y), data.services.opacity2, [1, 0], true);
  } else if (Math.abs(scroll.lerp.y) < data.services.min[0] || Math.abs(scroll.lerp.y) >= data.services.max[1]) {
    dom.services.style.opacity = 0;
    dom.services.classList.add('not-visible');
  }

  if (Math.abs(scroll.lerp.y) >= data.recycle.min[0] && Math.abs(scroll.lerp.y) < data.recycle.min[1]) {
    dom.recycle.style.opacity = modulate(Math.abs(scroll.lerp.y), data.recycle.opacity, [0, 1], true);
    dom.recycle.children[0].style.transform = `translateY(${modulate(Math.abs(scroll.lerp.y), data.recycle.translate, [100, 0], true)}px)`;
    dom.recycle.classList.remove('not-visible');
  } else if (Math.abs(scroll.lerp.y) >= data.recycle.max[0] && Math.abs(scroll.lerp.y) < data.recycle.max[1]) {
    dom.recycle.classList.remove('not-visible');
    dom.recycle.style.opacity = modulate(Math.abs(scroll.lerp.y), data.recycle.opacity2, [1, 0], true);
  } else if (Math.abs(scroll.lerp.y) < data.recycle.min[0] || Math.abs(scroll.lerp.y) >= data.recycle.max[1]) {
    dom.recycle.style.opacity = 0;
    dom.recycle.classList.add('not-visible');
  }

  if (Math.abs(scroll.lerp.y) >= data.contact.min) {
    dom.contact.style.opacity = modulate(Math.abs(scroll.lerp.y), data.contact.opacity, [0, 1], true);
    dom.ctas.style.opacity = modulate(Math.abs(scroll.lerp.y), data.contact.cta, [0, 1], true);
    dom.contact.classList.remove('not-visible');
  } else {
    dom.contact.classList.add('not-visible');
  }

  if (Math.abs(scroll.lerp.y) >= data.contact.cta[0]) {
    dom.ctas.style.pointerEvents = 'auto';
  } else {
    dom.ctas.style.pointerEvents = 'none';
  }
};

const rayFn = () => {
  stage.raycaster.setFromCamera({ x: mouse.ray.x, y: mouse.ray.y }, stage.camera );

  const intersects = stage.raycaster.intersectObjects( stage.scene.children[4].children );
  if (intersects.length > 0) {
    if (intersects[0].object.name === 'world') {
      intersect = intersects[0].object;

      document.body.classList.add('hover');

      if (!intersectSignal) {
        anime.remove(intersect);
      }
      intersectSignal = true;
    }
  } else {
    if (intersect && intersectSignal) {
      document.body.classList.remove('hover');

      intersect = null;
      intersectSignal = false;
    }
  }

  const intersectsPoster = stage.raycaster.intersectObjects( stage.scene.children[2].children );
  if (intersectsPoster.length > 0) {
    if (intersectsPoster[0].object.name === 'poster-1' || intersectsPoster[0].object.name === 'poster-2') {
      intersect = intersectsPoster[0].object;

      document.body.classList.add('hover');

      if (!intersectSignal) {
        intersect.material.depthTest = false;
      }
      intersectSignal = true;
    } else {
      if (intersect && intersectSignal) {
        document.body.classList.remove('hover');
        intersect.material.depthTest = true;
        intersect = null;
        intersectSignal = false;
      }
    }
  }
};

const trailFn = () => {
    dom.trail.style.transform = `translate3d(${mouse.x}px, ${mouse.y}px, 0)`;
  if (mouse.hover) {
    dom.trail.style.opacity = 1;
  } else {
    dom.trail.style.opacity = 0;
  }
};

dom.scrollWrapper.addEventListener('click', (e) => {
  e.preventDefault();
  const value = { y: 0 };
  anime({
    targets: value,
    y: -1500,
    duration: 1500,
    easing: 'linear',
    update: (anim) => {
      scroll.goTo(value.y);
    },
  });
});

document.body.addEventListener('mousemove', (e) => {
  const { clientX, clientY } = e;
  if (!data.touch) {
    mouse.x = clientX;
    mouse.y = clientY;
    mouse.hover = e.target.tagName === 'A' || e.target.tagName === 'H1' || e.target.tagName === 'H2' || e.target.tagName === 'H3' || document.body.classList.contains('hover');
  }

  mouse.ray.x = ( event.clientX / window.innerWidth ) * 2 - 1;
  mouse.ray.y = - ( event.clientY / window.innerHeight ) * 2 + 1;
});

document.body.addEventListener('click', (e) => {
  if (!intersect) return;
  if (intersect.name === 'world') {
    if (data.safari) {
      window.location.href = 'https://goo.gl/maps/Ux1r8Y3GsQgJVhY88';
    } else {
      window.open('https://goo.gl/maps/Ux1r8Y3GsQgJVhY88', '_blank');
    }
  } else if (intersect.name === 'poster-1' || intersect.name === 'poster-2') {
    if (!open.signal) {
      open.signal = true;
      open.name = intersect.name;
      open.scale = intersect.scale.x;
      open.position = {
        x: intersect.position.x,
        y: intersect.position.y,
        z: intersect.position.z,
      };

      intersect.material.depthTest = false;
      setTimeout(() => {
        intersect.material.opacity = 1;
      }, 1);
      dom.flagship.style.opacity = 0;
      dom.welcome.style.opacity = 0;

      anime({
        targets: intersect.scale,
        x: 0.8,
        y: 0.8,
        z: 0.8,
        easing: 'spring(1, 80, 10, 0)',
        duration: 3000,
      });
      anime({
        targets: intersect.position,
        x: 0,
        y: 0,
        z: stage.camera.position.z + 0.2,
        easing: 'spring(1, 80, 10, 0)',
        duration: 3000,
      });
    } else {
      open.signal = false;
      open.name = null;
      intersect.material.depthTest = true;
      dom.flagship.style.opacity = 1;
      dom.welcome.style.opacity = 1;
      anime({
        targets: intersect.scale,
        x: open.scale,
        y: open.scale,
        z: open.scale,
        easing: 'spring(1, 80, 10, 0)',
        duration: 3000,
      });
      anime({
        targets: intersect.position,
        x: open.position.x,
        y: open.position.y,
        z: open.position.z,
        easing: 'spring(1, 80, 10, 0)',
        duration: 3000,
      });
    }
  }
});

const transition = () => {
  anime({
    targets: dom.mask,
    opacity: [1, 0],
    easing: 'spring(1, 80, 10, 0)',
    duration: 600,
    complete: () => {
      dom.mask.style.display = 'none';
    }
  });

  anime({
    targets: document.querySelectorAll('.claim bdi'),
    opacity: [0, 1],
    translateY: [100, 0],
    easing: 'spring(1, 80, 10, 0)',
    duration: 1600,
    delay: anime.stagger(200, { start: 1400 }),
  });

  anime({
    targets: document.querySelector('.logo'),
    opacity: [0, 1],
    translateY: [30, 0],
    easing: 'spring(1, 80, 10, 0)',
    duration: 1400,
    delay: 600,
  });

  anime({
    targets: document.querySelectorAll('.socials li'),
    opacity: [0, 1],
    translateY: [30, 0],
    easing: 'spring(1, 80, 10, 0)',
    duration: 1400,
    delay: anime.stagger(300, { start: 1000 }),
  });

  anime({
    targets: dom.scroll,
    opacity: [0, 1],
    translateY: [30, 0],
    easing: 'spring(1, 80, 10, 0)',
    duration: 1400,
    delay: 1000,
  });

  anime({
    targets: dom.scrollPaths,
    opacity: [0, 1],
    loop: true,
    direction: 'alternative',
    delay: anime.stagger(300),
    easing: 'spring(1, 80, 10, 0)',
  });
};