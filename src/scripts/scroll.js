import normalizeWheel from 'normalize-wheel';
import Momentum from '../lib/momentum';
import { lerp as lerpFn } from './utils';
import stage from './stage';
import data from './costants';
import aion from './aion';
import { open, status } from './';

let y = 0;
let scrollto = 0;
const lerp = {
  y: 0,
  prev: {
    y: 0,
  },
};

if (!data.touch) {
  document.addEventListener('wheel', (event) => {
    if (open.name !== null || !status.init) return;
    const normalized = normalizeWheel(event);

    y += Math.min(70, Math.max(-70, normalized.pixelY));

    y = Math.max(Math.min(y, data.viewport.min), data.viewport.max);

    lerp.y = lerped(y);
    lerp.prev.y = lerp.y;
  });

} else {
  const momentum = new Momentum({
    axis: 'y',
    max: () => (-data.viewport.max + scrollto),
    min: () => (data.viewport.min + scrollto),
    view: document.body,
    onChange: (y) => {
      if (open.name !== null) return;

      lerp.y = lerped((y - scrollto) * -1);
      lerp.prev.y = lerp.y;
    },
  });
}

const goTo = (x) => {
  y = x;
  scrollto = x;
  lerp.y = lerped(y);
  lerp.prev.y = lerp.y;
};

const lerped = (y) => {
  return Math.round(lerpFn(y, lerp.y, 0.8));
};

export {
  y,
  lerp,
  goTo,
};
