import Stage from '../lib/stage';
import aion from './aion';

const stage = new Stage(
  document.querySelector('#app'),
  aion
);

export default stage;