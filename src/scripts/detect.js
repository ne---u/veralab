import { getGPUTier } from 'detect-gpu';
import checkPerf from 'check-performance';
import { data } from './costants';

const GPUTier = getGPUTier({
  mobileBenchmarkPercentages: [0, 50, 30, 20],
  desktopBenchmarkPercentages: [0, 50, 30, 20],
});

const { tier, type } = GPUTier;

export const detect = (tier === 'GPU_DESKTOP_TIER_0'
  || tier === 'GPU_DESKTOP_TIER_1'
  || tier === 'GPU_MOBILE_TIER_1'
  || tier === 'GPU_MOBILE_TIER_0'
  || tier === 'GPU_MOBILE_TIER_2')
  && type !== 'FALLBACK';

const onChange = (level) => {
  if (typeof level !== 'undefined') {
    data.detect.level = level;
  }
  return level;
};

const { update: updateCheckPerf } = checkPerf(onChange, {
  maxLevel: 100,
  maxFps: 45,
});

export default updateCheckPerf;
