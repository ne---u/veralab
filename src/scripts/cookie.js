import Cookies from 'js-cookie';
import anime from 'animejs';

class Cookie {
  constructor(el) {
    this.el = el;
    this.button = el.querySelector('button');
    this.onAcceptClickHandler = () => this.onAcceptClick();
  }

  init() {
    if (Cookies.get('acceptCookie')) {
      this.hide();
    } else {
      this.button.addEventListener('click', this.onAcceptClickHandler);
      this.fadeIn();
    }
  }

  onAcceptClick() {
    this.fadeOut();
    Cookies.set('acceptCookie', true, { expires: 365 });
  }

  hide() {
    this.el.parentNode.style.display = 'none';
  }

  fadeOut() {
    anime({
      targets: this.el,
      opacity: 0,
      translateY: 200,
      easing: 'spring(1, 80, 10, 0)',
      duration: 1600,
      delay: 400,
    });
  }

  fadeIn() {
    anime({
      targets: this.el,
      opacity: [0, 1],
      translateY: [200, 0],
      easing: 'spring(1, 80, 10, 0)',
      duration: 1800,
      delay: 400,
    });
  }
} 

export default Cookie;
