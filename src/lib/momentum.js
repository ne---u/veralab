// https://ariya.io/2013/11/javascript-kinetic-scrolling-part-2
export default (options) => {
  const {
    axis = 'y',
    view = window,
    max = Infinity,
    min = 0,
    inverse = false,
    preventDefault = false,
    onChange = () => { },
  } = options;

  let offset = 0;
  let reference;
  let pressed = false;
  let velocity;
  let frame;
  let timestamp;
  let ticker;
  let amplitude;
  let target;
  const timeConstant = 325; // ms

  function getMin() {
    if (typeof min === 'function') return min();
    return min;
  }

  function getMax() {
    if (typeof max === 'function') return max();
    return max;
  }

  function scroll(value) {
    // eslint-disable-next-line no-nested-ternary
    offset = (value > getMax()) ? getMax() : (value < getMin()) ? getMin() : value;
    onChange(inverse ? -offset : offset);
  }

  function pos(e) {
    const property = [`client${axis.toUpperCase()}`];
    let event = e;
    if (e.targetTouches && (e.targetTouches.length >= 1)) {
      [event] = e.targetTouches;
    }
    return event[property];
  }

  function track() {
    const now = Date.now();
    const elapsed = now - timestamp;
    const delta = offset - frame;
    const v = 1000 * delta / (1 + elapsed);

    timestamp = now;
    frame = offset;
    velocity = 0.8 * v + 0.2 * velocity;
  }

  function autoScroll() {
    let elapsed;
    let delta;

    if (amplitude) {
      elapsed = Date.now() - timestamp;
      delta = -amplitude * Math.exp(-elapsed / timeConstant);
      if (delta > 0.5 || delta < -0.5) {
        scroll(target + delta);
        requestAnimationFrame(autoScroll);
      } else {
        scroll(target);
      }
    }
  }

  function tap(e) {
    pressed = true;
    reference = pos(e);

    // eslint-disable-next-line no-multi-assign
    velocity = amplitude = 0;
    frame = offset;
    timestamp = Date.now();
    clearInterval(ticker);
    ticker = setInterval(track, 40);

    if (preventDefault) {
      e.preventDefault();
    }
    return false;
  }

  function drag(e) {
    let y;
    let delta;
    if (pressed) {
      y = pos(e);
      delta = (reference - y);
      if (delta > 2 || delta < -2) {
        reference = y;
        scroll(offset + delta);
      }
    }
    if (preventDefault) {
      e.preventDefault();
    }
    return false;
  }

  function release(e) {
    pressed = false;

    clearInterval(ticker);
    target = offset;
    if (velocity > 10 || velocity < -10) {
      amplitude = 0.8 * velocity;
      target = Math.round(offset + amplitude);
      // eslint-disable-next-line no-nested-ternary
      target = target < getMin() ? getMin() : target > getMax() ? getMax() : target;
      amplitude = target - offset;
      timestamp = Date.now();
      requestAnimationFrame(autoScroll);
    }

    if (preventDefault) {
      e.preventDefault();
    }
    return false;
  }

  function manageEvent(remove) {
    const fn = remove ? 'removeEventListener' : 'addEventListener';
    if (typeof window.ontouchstart !== 'undefined') {
      view[fn]('touchstart', tap);
      view[fn]('touchmove', drag);
      view[fn]('touchend', release);
    }
    view[fn]('mousedown', tap);
    view[fn]('mousemove', drag);
    view[fn]('mouseup', release);
  }

  manageEvent();

  return {
    destroy: () => {
      manageEvent(true);
    },
  };
};
