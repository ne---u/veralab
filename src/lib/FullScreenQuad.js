import * as THREE from 'three';

/* eslint-disable no-undef, func-names */
THREE.FullScreenQuad = (function () {
  const camera = new THREE.OrthographicCamera(-1, 1, 1, -1, 0, 1);
  const geometry = new THREE.PlaneBufferGeometry(2, 2);

  const FullScreenQuad = function (material) {
    this.mesh = new THREE.Mesh(geometry, material);
    this.mesh.frustumCulled = false;
  };

  Object.defineProperty(FullScreenQuad.prototype, 'material', {
    get() {
      return this.mesh.material;
    },

    set(value) {
      this.mesh.material = value;
    },
  });

  Object.assign(FullScreenQuad.prototype, {
    render(renderer) {
      renderer.render(this.mesh, camera);
    },
  });

  return FullScreenQuad;
}());
