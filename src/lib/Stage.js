import * as THREE from "three";
import { GLTFLoader } from "three/examples/jsm/loaders/GLTFLoader";
import anime from "animejs";
import Bloom from "./Bloom";
import * as scroll from "../scripts/scroll";
import data from "../scripts/costants";
import aion from "../scripts/aion";
import "./FullScreenQuad";

class Stage {
  constructor(dom, aion) {
    this.dom = dom;
    this.ratio = window.innerWidth / window.innerHeight;
    (this.raf = aion), (this.scene = new THREE.Scene());
    this.scene2 = new THREE.Scene();
    this.scene3 = new THREE.Scene();

    this.camera = new THREE.PerspectiveCamera(35, this.ratio, 0.1, 2);

    this.renderer = new THREE.WebGLRenderer({
      // antialias: true,
      premultipliedAlpha: true
    });
    this.renderTarget = new THREE.WebGLRenderTarget({
      format: THREE.RGBFormat,
      depthBuffer: false,
      stencilBuffer: false
    });
    this.renderTarget2 = new THREE.WebGLRenderTarget({
      format: THREE.RGBFormat,
      depthBuffer: false,
      stencilBuffer: false
    });

    this.bloom = new Bloom(this.renderer, this.renderTarget, {
      resx: window.innerWidth,
      resy: window.innerHeight
    });

    this.materials = {
      matcap: new THREE.MeshMatcapMaterial()
    };

    this.rendererHandler = () => {
      if (
        (data.detect.level < 10 || data.detect.tier) || Math.abs(scroll.lerp.y) > data.services.min[0]
        || (
          Math.abs(scroll.lerp.y) > 300 && Math.abs(scroll.lerp.y) < data.bloom.min
        )
      ) {
        this.renderer.setRenderTarget(null);
        this.changeBg();
        this.renderer.clear();
        this.scene.fog = this.fog();
        this.renderer.render(this.scene, this.camera);
      } else {
        this.renderer.setRenderTarget(this.renderTarget);
        this.renderer.setClearColor(0, 0, 0, 0);
        this.renderer.clear();
        this.scene.fog = null;
        this.scene.traverse(darkenNonBloomed);
        this.renderer.render(this.scene, this.camera);
        this.scene.traverse(restoreMaterial);
        this.renderer.setRenderTarget(this.renderTarget2);
        this.changeBg();
        this.renderer.clear();
        this.scene.fog = this.fog();
        this.renderer.render(this.scene, this.camera);
        this.bloom.render(this.renderTarget2);
      }
    };

    const darkMaterial = new THREE.MeshBasicMaterial({ color: 0x000000 });
    const materials = {};
    function darkenNonBloomed(obj) {
      if (obj.isMesh && obj.name !== "Bloom" && obj.name !== "Sweep") {
        if (obj.name === "Circle") {
          materials[obj.uuid] = obj.material;
          obj.material = darkMaterial;
        } else {
          obj.visible = false;
        }
      }
    }

    function restoreMaterial(obj) {
      obj.visible = true;
      if (materials[obj.uuid]) {
        obj.material = materials[obj.uuid];
        delete materials[obj.uuid];
      }
    }

    this.light = null;

    this.loader = {
      texture: new THREE.TextureLoader(),
      gltf: new GLTFLoader()
    };

    this.colors = {
      pink: new THREE.Color(0xf291c8),
      pink3: new THREE.Color(0xf7b4c6),
      pink4: new THREE.Color(0xb61473),
      magenta: new THREE.Color(0xff9ad2),
      magenta2: new THREE.Color(0xff6ebf),
      yellow: new THREE.Color(0xffe1a9),
      white: new THREE.Color(0xffffff),
      violet: new THREE.Color(0x903e6d),
      lille: new THREE.Color(0xe8a0e5),
      pink33: new THREE.Color(0xE25EA6),
    };

    this.raycaster = new THREE.Raycaster();

    // this.clock = new THREE.Clock();

    this.groups = {};

    this.init();

    this.debug = false;
    this.signal = "magenta";
    this.background = new THREE.Color(0xff9ad2);

    this.anime = {
      pink: anime({
        targets: this.background,
        r: this.colors.pink3.r,
        g: this.colors.pink3.g,
        b: this.colors.pink3.b,
        duration: 1000,
        easing: "linear",
        autoplay: false
      }),
      magenta: anime({
        targets: this.background,
        r: this.colors.magenta.r,
        g: this.colors.magenta.g,
        b: this.colors.magenta.b,
        duration: 1000,
        easing: "linear",
        autoplay: false
      }),
      pink3: anime({
        targets: this.background,
        r: this.colors.pink33.r,
        g: this.colors.pink33.g,
        b: this.colors.pink33.b,
        duration: 1000,
        easing: "linear",
        autoplay: false
      }),
      pink4: anime({
        targets: this.background,
        r: this.colors.pink.r,
        g: this.colors.pink.g,
        b: this.colors.pink.b,
        duration: 1000,
        easing: "linear",
        autoplay: false
      }),
    };
  }

  init() {
    this.setSize();
    this.scene.add(this.camera);
    this.renderer.autoClear = false;

    this.dom.appendChild(this.renderer.domElement);
    const resizeHandler = () => this.setSize();
    window.addEventListener("resize", resizeHandler, false);

    this.fog();
    this.lights();

    this.camera.position.set(0, 0, 0);
  }

  setSize() {
    this.ratio = window.innerWidth / window.innerHeight;
    if (this.camera) {
      this.camera.aspect = this.ratio;
      this.camera.updateProjectionMatrix();
    }

    // const bloomSize = {
    //   width: window.innerWidth <= 1280 ? 375 : window.innerWidth,
    //   height: window.innerWidth <= 1280 ? window.innerHeight : window.innerHeight,
    // };

    const pixelRatio = 1;
    const bloom = {
      width: window.innerWidth > 512 ? window.innerWidth * 0.75 : window.innerWidth,
      height: window.innerWidth > 512 ? window.innerHeight * 0.75 : window.innerHeight,
    };

    this.renderer.setPixelRatio(pixelRatio);
    this.renderer.setSize(window.innerWidth, window.innerHeight);
    this.renderTarget.setSize(window.innerWidth, window.innerHeight);
    this.renderTarget2.setSize(window.innerWidth, window.innerHeight);

    this.bloom.setSize(bloom.width, bloom.height);
  }

  manager(type, assets, cb) {
    const promises = [];
    assets.map(asset => {
      const promise = new Promise(resolve =>
        this.loader[type].load(asset, resolve)
      );
      promises.push(promise);
    });
    return promises;
  }

  lights() {
    const color = this.colors.pink3;
    const intensity = 1;
    this.light = new THREE.DirectionalLight(color, intensity);
    this.light.position.set(-0.5, 0, 5);
    this.scene.add(this.light);
  }

  fog() {
    if (!this.fogInstance) {
      this.fogInstance = new THREE.FogExp2(this.colors.magenta, 1);
    }
    return this.fogInstance;
  }

  setDebug() {
    window.THREE = THREE;
    window.scene = this.scene;
  }

  group(name, position) {
    const group = new THREE.Group();
    group.name = name;

    Object.assign(this.groups, {
      [name]: group
    });

    Object.assign(group.position, new THREE.Vector3(...position));

    this.scene.add(group);
  }

  add(item, add = true) {
    const { geometry: geo, material: mat, mesh: mes } = item;
    const geometry = new THREE[geo.type](...Object.values(geo.params));
    const material = new THREE[mat.type](mat.params);
    const mesh = new THREE.Mesh(geometry, material);

    Object.keys(mes).map(param =>
      mesh[param].set(...Object.values(mes[param]))
    );

    if (typeof add === "string") {
      this.groups[add].add(mesh);
    } else if (add) {
      this.scene.add(mesh);
    }

    return mesh;
  }

  changeBg() {
    if (
      this.signal === "pink" &&
      Math.abs(scroll.lerp.y) < data.services.min[0]
    ) {
      anime.remove(this.background);
      this.signal = "magenta";
      this.anime.magenta.play();
    }
    if (
      (Math.abs(scroll.lerp.y) >= data.services.min[0] &&
        this.signal === "magenta") ||
      (Math.abs(scroll.lerp.y) < data.recycle.min[0] && this.signal === "pink3")
    ) {
      anime.remove(this.background);
      this.signal = "pink";
      this.anime.pink.play();
    }
    if (
      (Math.abs(scroll.lerp.y) >= data.recycle.min[0] &&
        this.signal === "pink") ||
      (Math.abs(scroll.lerp.y) < data.contact.min && this.signal === "pink4")
    ) {
      anime.remove(this.background);
      this.signal = "pink3";
      this.anime.pink3.play();
    }
    if (Math.abs(scroll.lerp.y) >= data.contact.min && this.signal === "pink3") {
      anime.remove(this.background);
      this.signal = "pink4";
      this.anime.pink4.play();
    }

    this.renderer.setClearColor(this.background, 1);
  }
}

export default Stage;
