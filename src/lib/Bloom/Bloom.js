import * as THREE from "three";
import vertexShader from "./shaders/vertex.vert";
import brightShader from "./shaders/bright.frag";
import blurShader from "./shaders/blur.frag";
import bloomShader from "./shaders/bloom.frag";

import data from '../../scripts/costants';

export default class {
  constructor(
    renderer,
    target,
    {
      luminosityThreshold = 0.002,
      radius = data.touch ? 1.5 : 2.0,
      strength = data.touch ? 0.2 : 2.5,
      resx = 300,
      resy = 300
    }
  ) {
    this.width = resx;
    this.height = resy;
    this.renderer = renderer;
    this.fsQuad = new THREE.FullScreenQuad(null);

    const pars = {
      minFilter: THREE.LinearFilter,
      magFilter: THREE.LinearFilter,
      format: THREE.RGBFormat,
      depthBuffer: false,
      stencilBuffer: false
    };

    this.kernelSizeArray = [3, 5, 7, 9, 11];

    this.renderTargetBright = new THREE.WebGLRenderTarget(
      this.width,
      this.height,
      pars
    );
    this.renderTargetFinal = new THREE.WebGLRenderTarget(
      this.width,
      this.height,
      pars
    );

    this.rtsHorizontal = [];
    this.rtsVertical = [];

    this.i = data.touch ? 5 : 5;

    for (let i = 0; i < this.i; i++) {
      const renderTargetHorizonal = new THREE.WebGLRenderTarget(
        this.width,
        this.height,
        pars
      );
      renderTargetHorizonal.texture.name = `AQBloomPass.h${i}`;
      renderTargetHorizonal.texture.generateMipmaps = false;

      this.rtsHorizontal.push(renderTargetHorizonal);

      const renderTargetVertical = new THREE.WebGLRenderTarget(
        this.width,
        this.height,
        pars
      );
      renderTargetVertical.texture.name = `AQBloomPass.v${i}`;
      renderTargetVertical.texture.generateMipmaps = false;

      this.rtsVertical.push(renderTargetVertical);

      this.width = Math.round(this.width / 2);

      this.height = Math.round(this.height / 2);
    }

    this.material = {
      bright: new THREE.RawShaderMaterial({
        uniforms: {
          luminosityThreshold: { value: luminosityThreshold },
          texture: { value: target.texture },
          smoothWidth: { value: 0.02 }
        },
        vertexShader,
        fragmentShader: brightShader
      }),
      blur: new THREE.RawShaderMaterial({
        uniforms: {
          resolution: {
            value: new THREE.Vector2(window.innerWidth, window.innerHeight)
          },
          direction: { value: new THREE.Vector2(1.0, 0.0) },
          krs: { value: this.kernelSizeArray[0] },
          texture: { value: null }
        },
        vertexShader,
        fragmentShader: blurShader
      }),
      bloom: new THREE.RawShaderMaterial({
        defines: { NUM_MIPS: this.i },
        uniforms: {
          blurTexture1: { value: this.rtsVertical[0].texture },
          blurTexture2: { value: this.rtsVertical[1].texture },
          blurTexture3: { value: this.rtsVertical[2].texture },
          blurTexture4: { value: this.rtsVertical[3].texture },
          blurTexture5: { value: this.rtsVertical[4].texture },
          targetTexture: { value: target.texture },
          bloomStrength: { value: strength },
          bloomRadius: { value: radius },
          bloomFactors: { value: [1.0, 0.6, 0.4, 0.2, 0.1] },
          bloomTintColors: {
            value: [
              new THREE.Vector3(1, 1, 1),
              new THREE.Vector3(1, 1, 1),
              new THREE.Vector3(1, 1, 1),
              new THREE.Vector3(1, 1, 1),
              new THREE.Vector3(1, 1, 1)
            ]
          }
        },
        vertexShader,
        fragmentShader: bloomShader,
        transparent: true
      }),
      final: new THREE.ShaderMaterial({
        uniforms: {
          baseTexture: { value: null },
          bloomTexture: { value: null }
        },
        vertexShader: `
        varying vec2 vUv;
        void main() {
          vUv = uv;
          gl_Position = projectionMatrix * modelViewMatrix * vec4( position, 1.0 );
        }`,
        fragmentShader: `
        uniform sampler2D baseTexture;
        uniform sampler2D bloomTexture;
        varying vec2 vUv;
        vec4 getTexture( sampler2D texelToLinearTexture ) {
          return mapTexelToLinear( texture2D( texelToLinearTexture , vUv ) );
        }
        void main() {
          gl_FragColor = ( getTexture( baseTexture ) + vec4( 1.0 ) * getTexture( bloomTexture ) );
        }`,
        defines: {}
      })
    };
  }

  setSize(w, h) {
    this.renderTargetBright.setSize(w, h);
    for (let i = 0; i < this.i; i++) {
      this.rtsHorizontal[i].setSize(w, h);
      this.rtsVertical[i].setSize(w, h);
    }
  }

  // dispose = () => {
  //   for (let i = 0; i < 5; i++) {
  //     this.rtsHorizontal[i].dispose();
  //     this.rtsVertical[i].dispose();
  //   }

  //   this.renderTargetBright.dispose();
  //   this.material.bloom.dispose();
  //   this.material.bright.dispose();
  //   this.material.blur.dispose();
  // };

  render(final = null) {
    this.renderer.setRenderTarget(this.renderTargetBright);
    this.fsQuad.material = this.material.bright;
    this.fsQuad.render(this.renderer);

    this.fsQuad.material = this.material.blur;
    let inputRenderTarget = this.renderTargetBright;

    for (let i = 0; i < this.i; i++) {
      this.renderer.setRenderTarget(this.rtsHorizontal[i]);
      this.fsQuad.material.uniforms.texture.value = inputRenderTarget.texture;
      this.fsQuad.material.uniforms.direction.value.set(1.0, 0.0);
      this.fsQuad.material.uniforms.krs.value = this.kernelSizeArray[i];
      this.fsQuad.render(this.renderer);

      this.renderer.setRenderTarget(this.rtsVertical[i]);
      this.fsQuad.material.uniforms.texture.value = this.rtsHorizontal[
        i
      ].texture;
      this.fsQuad.material.uniforms.direction.value.set(0.0, 1.0);
      this.fsQuad.material.uniforms.krs.value = this.kernelSizeArray[i];

      this.fsQuad.render(this.renderer);

      inputRenderTarget = this.rtsVertical[i];
    }

    this.renderer.setRenderTarget(final ? this.renderTargetFinal : null);
    this.renderer.setClearColor(0,0,0,0);
    this.renderer.clear();
    this.fsQuad.material = this.material.bloom;
    this.fsQuad.render(this.renderer);

    if(final) {
      this.renderer.setRenderTarget(null);
      this.fsQuad.material = this.material.final;
      this.fsQuad.material.uniforms.bloomTexture.value = this.renderTargetFinal.texture; 
      this.fsQuad.material.uniforms.baseTexture.value = final.texture; 
      this.fsQuad.render(this.renderer);
    }
  }
}
