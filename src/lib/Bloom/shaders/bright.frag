precision highp float; 
uniform sampler2D texture; 
uniform float luminosityThreshold; 
uniform float smoothWidth; 
varying vec2 vUv; 
void main() {
  vec3 texel = texture2D(texture, vUv).rgb;
  vec3 luma = vec3(0.299, 0.587, 0.114);
  float v = dot(texel, luma);
  vec4 outputColor = vec4(0.0, 0.0, 0.0, 1.0);
  float alpha = smoothstep(luminosityThreshold, luminosityThreshold + smoothWidth, v);
  outputColor.rgb = mix(outputColor.rgb, texel, alpha);
  gl_FragColor = outputColor;
}
