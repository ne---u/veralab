import { scene as Scene1 } from './scene1';
import { scene as Scene2 } from './scene2';
import { scene as Scene3 } from './scene3';

export {
  Scene1,
  Scene2,
  Scene3,
};