import * as THREE from "three";
import anime from "animejs";

import stage from "../../scripts/stage";
import aion from "../../scripts/aion";
import * as scroll from "../../scripts/scroll";
import { modulate } from "../../scripts/utils";
import data from "../../scripts/costants";
import { MeshBasicMaterial, MeshMatcapMaterial } from "three";

export const scene = {};

export const wall = (model, matcap, textures) => {
  const { scene } = model;

  const basicMaterial = stage.materials.matcap;
  basicMaterial.matcap = matcap;

  const framesPos = [
    {
      x: -0.55,
      y: 0.84,
      z: 0
    },
    {
      x: -1.92,
      y: -1.6,
      z: 0
    },
    {
      x: 4.62,
      y: -0.93,
      z: 0
    },
    {
      x: 2.52,
      y: 0.75,
      z: 0
    },
    {
      x: 1.24,
      y: -1.8,
      z: 0
    }
  ];

  const group = new THREE.Group();
  stage.groups.scene2.add(group);
  group.position.set(-0.05, 0.25, 0.0);
  group.scale.set(0.055, 0.055, 0.055);
  group.rotation.set(-90 * THREE.Math.DEG2RAD, 0, 0);
  group.frustumCulled = false;

  scene.traverse(child => {
    if (child.type === "Scene") return;
    child.material = basicMaterial.clone();
    const temp = child.clone();
    let texture = null;

    switch (child.name) {
      case "Circle":
        temp.material.color = new THREE.Color(0xe835a9);
        break;
      case "Bloom":
        temp.material.opacity = 0.05;
        temp.material.transparent = true;
        temp.material.color = stage.colors.white;
        temp.material.matcap = null;

        aion.add(
          () => {
            temp.material.opacity = modulate(
              Math.abs(scroll.lerp.y),
              [data.bloom.min, data.bloom.max],
              [0, (data.detect.level < 10 || data.detect.tier) || data.touch ? 0.5 : 0.04],
              true
            );
          },
          'bloom',
          true
        );

        break;
      case "Cylinder":
      case "Cylinder009":
      case "Cylinder001":
        texture = child.name === 'Cylinder009' ? textures[0] : textures[1];
        texture.anisotropy = data.touch ? 2 : stage.renderer.capabilities.getMaxAnisotropy();
        texture.flipY = false;
        temp.material.map = texture;
        break;
      case "Cylinder002":
      case "Cylinder006":
        texture = textures[2];
        texture.anisotropy == data.touch ? 2 : stage.renderer.capabilities.getMaxAnisotropy();
        texture.flipY = false;
        temp.material.map = texture;
        break;
      case "Cylinder005":
      case "Cylinder007":
        temp.material.transparent = true;
        temp.material.opacity = 0.4; 
        break;
      case "Cube001":
      case "Cube002":
      case "Cube003":
      case "Cube000":
        texture = textures[3];
        texture.anisotropy == data.touch ? 2 : stage.renderer.capabilities.getMaxAnisotropy();

        texture.flipY = false;
        temp.material.map = texture;
        break;
    }

    const mesh = new THREE.InstancedMesh(child.geometry, child.material, 5);

    mesh.position.copy(child.position);
    mesh.name = child.name;
    mesh.frustumCulled = false;

    group.add(mesh);
  });

  const dummy = new THREE.Object3D();

  framesPos.forEach((n, i) => {
    group.traverse(child => {
      child.updateMatrix();
      child.updateWorldMatrix();
      if (child.isMesh) {
        dummy.position.set(n.x, n.z, n.y);
        dummy.scale.copy(child.scale);
        dummy.rotation.copy(child.rotation);
        dummy.updateMatrix();
        child.setMatrixAt(i, dummy.matrix);
      }
    });
  });
};

Object.assign(scene, {
  wall
});
