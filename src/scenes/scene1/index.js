import * as THREE from "three";
import anime from "animejs";

import stage from "../../scripts/stage";
import aion from "../../scripts/aion";
import * as scroll from "../../scripts/scroll";

import { modulate } from "../../scripts/utils";
import data from "../../scripts/costants";
import { open } from '../../scripts/';

export const scene = {};

const floor = texture => {

  texture.wrapS = THREE.RepeatWrapping;
  texture.wrapT = THREE.RepeatWrapping;
  texture.repeat.set(6, 9);
  texture.offset.x = 0.25;
  texture.anisotropy = stage.renderer.capabilities.getMaxAnisotropy();

  const mesh = stage.add(
    {
      geometry: {
        type: "PlaneBufferGeometry",
        params: {
          width: 0,
          height: 0,
          widthSegments: 32,
          heightSegments: 32
        }
      },

      material: {
        type: "MeshBasicMaterial",
        params: {
          map: texture
        }
      },
      mesh: {
        position: {
          x: 0,
          y: 0,
          z: 0
        },
        scale: {
          x: 1.5,
          y: 4.6,
          z: 1,
        },
        rotation: {
          x: -89.9 * THREE.Math.DEG2RAD,
          y: 0,
          z: 0,
      }
    }
    },
    "scene1"
  );
  const bounding = mesh.geometry.computeBoundingBox();
  mesh.geometry.translate(0, mesh.geometry.boundingBox.max.y / 2, 0);

  const y = window.innerWidth > 768 ? -.15 : -.1;

  anime({
    targets: mesh.position,
    y: [-0.6, y],
    easing: 'easeInOutExpo',
    duration: 2000,
  });

  var positionAttribute = mesh.geometry.attributes.position;
  var pos = mesh.geometry.attributes.position;

  const wavesBuffer = () => {
    pos.needsUpdate = true;
    const theTime = performance.now() * .0018;

    let center = new THREE.Vector3(0.15, 0.15, 0.15);
    var vec3 = new THREE.Vector3();
    for ( var i = 0, l = pos.count; i < l; i ++ ) {
      vec3.fromBufferAttribute(pos, i);
      vec3.sub(center);
      var z = 0.003 * Math.sin(vec3.length() / -.01 + (theTime)) * 1.2;
      pos.setZ(i, z);
    }
  };

  aion.add(() => {
    if ((data.detect.level > 10 || !data.detect.tier) && Math.abs(scroll.lerp.y) <= data.services.min[0]) {
      wavesBuffer();
    } 
  }, 'wave');


  mesh.frustumCulled = false;

  return mesh;
};

const door = (assets, matcap) => {
  const { scene } = assets;
  
  const wall = scene.children[0];
  const neon = scene.children[1];


  if (window.innerWidth <= 768) {
    wall.scale.set(.95, .95, .95);
    wall.position.set(0, -.1, .16);

    neon.scale.set(.95, .95, .95);
    neon.position.set(0, -.1, .17);
  } else {
    wall.scale.set(1, 1, 1);
    wall.position.set(0, -.15, .7);

    neon.scale.set(1, 1, 1);
    neon.position.set(0, -.15, .71);
  }

  neon.material = new THREE.MeshBasicMaterial({color: stage.colors.white });
  neon.material.transparent = true;
  neon.material.opacity = 0;

  wall.material.transparent = true;
  wall.material.opacity = 0;

  const material = new THREE.MeshMatcapMaterial({
    matcap,
    color: stage.colors.pink4,
    transparent: true
  });

  wall.material = material;

  anime({
    targets: wall.material,
    opacity: [0, 1],
    easing: "easeInOutExpo",
    duration: 3500
  });

  anime({
    targets: [neon.material],
    opacity: [0, (data.detect < 10 || data.detect.tier) || data.touch ? 1 : 0.12],
    easing: "easeInOutExpo",
    duration: 3000,
    delay: 1200,
  });

  const group = new THREE.Group();
  stage.groups.scene1.add(wall);
  stage.groups.scene1.add(neon);

  if (window.innerWidth < 768) {
    const geo = new THREE.PlaneBufferGeometry(1, 0.3);
    const plane = new THREE.Mesh(geo, material);
    plane.position.set(0, 0.28, .16);
    plane.name = 'plane';
    stage.groups.scene1.add(plane);
  }

  return wall;
};

const posters = (textures) => {
  const texture1 = textures[1];
  texture1.wrapS = THREE.RepeatWrapping;
  texture1.wrapT = THREE.RepeatWrapping;

  const texture2 = textures[0];
  texture2.wrapS = THREE.RepeatWrapping;
  texture2.wrapT = THREE.RepeatWrapping;

  let mesh = {
    scale: 0,
  };

  mesh = stage.add({
    geometry: {
      type: 'PlaneBufferGeometry',
      params: {
        width: window.innerWidth > 768 ? 0.5 : 0.3,
        height: window.innerWidth > 768 ? 0.5 : 0.3,
        widthSegments: 1,
        heightSegments: 1,
      }
    },
    material: {
      type: 'MeshBasicMaterial',
      params: {
        map: texture1,
        transparent: true,
        opacity: 0,
      }
    },
    mesh: {
      position: {
        x: window.innerWidth > 768 ? -.16 : -.08,
        y: -.04,
        z: -1,
      },
      scale: {
        x: .25,
        y: .25,
        z: .25,
      },
    },
  }, 'scene1');

  mesh.name = 'poster-2';

  const meshVideo = stage.add(
    {
      geometry: {
        type: "PlaneBufferGeometry",
        params: {
          width: window.innerWidth > 768 ? 0.5 : 0.3,
          height: window.innerWidth > 768 ? 0.5 : 0.3,
          widthSegments: 1,
          heightSegments: 1
        }
      },
      material: {
        type: "MeshBasicMaterial",
        params: {
          map: texture2,
          transparent: true,
          opacity: 0
        }
      },
      mesh: {
        position: {
          x: 0.14,
          y: -0.05,
          z: -2.22
        },
        scale: {
          x: .22,
          y: .22,
          z: .22,
        }
      }
    },
    "scene1"
  );

  meshVideo.name = 'poster-1';

  if (window.innerWidth <= 768) {
    meshVideo.position.set(0.05, 0.03, -2.22);
  }

  let signal = false;

  aion.add(() => {
    if (!(open && open.name === 'poster-1')) meshVideo.material.opacity = modulate(Math.abs(scroll.lerp.y), data.posters.video, [0, 1], true);
    if (!(open && open.name === 'poster-2')) mesh.material.opacity = modulate(Math.abs(scroll.lerp.y), data.posters.img, [0, 1], true);
  }, 'enter', true);
};

Object.assign(scene, {
  floor,
  door,
  posters
});
