import * as THREE from "three";
import anime from "animejs";
import stage from '../../scripts/stage';
import aion from '../../scripts/aion';
import * as scroll from '../../scripts/scroll';
import { modulate } from '../../scripts/utils';

import data from "../../scripts/costants";

export const scene = {};

const product = (model, matcap, texture) => {
  const mesh = model.scene.children[0];

  texture.flipY = false;
  texture.anisotropy = 3;

  const material = new THREE.MeshMatcapMaterial({
    map: texture,
    matcap,
    color: stage.colors.white,
    depthTest: true,
    depthWrite: true
  });

  mesh.material = material;

  if (window.innerWidth > 768) {
    mesh.position.set(-.16, -.45, 0);
    mesh.scale.set(.4, .4, .35);
  } else {
    mesh.position.set(-.06, -.45, 0);
    mesh.scale.set(.3, .3, .25);
  }
  mesh.rotation.set(-1.6, .28, 2.94);
  stage.groups.scene3.add(mesh);

  let time = 0;
  let offset = 0;

  aion.add(() => {
    time += 0.02;
    mesh.rotation.z = 0.4 + time;

    if (Math.abs(scroll.lerp.y) < data.services.scale[0]) {
      offset = stage.camera.position.y;
    }

    mesh.position.y = -0.45 + ((stage.camera.position.y - offset) * 0.7);
  }, 'b');
};

const recycle = (textures) => {
  const texture1 = new THREE.VideoTexture(textures[0]);
  texture1.minFilter = THREE.LinearFilter;
  texture1.magFilter = THREE.LinearFilter;
  texture1.format = THREE.RGBFormat;

  let mesh = {
    scale: 0,
  };

  mesh = stage.add({
    geometry: {
      type: 'CircleBufferGeometry',
      params: {
        radius: window.innerWidth > 768 ? 0.25 : 0.25,
        segments: 60
      }
    },
    material: {
      type: 'MeshBasicMaterial',
      params: {
        map: texture1,
      }
    },
    mesh: {
      position: {
        x: window.innerWidth > 768 ? -.05 : -.05,
        y: -1.8,
        z: 0,
      },
      scale: {
        x: window.innerWidth > 768 ? .4 : .3,
        y: window.innerWidth > 768 ? .4 : .3,
        z: window.innerWidth > 768 ? .4 : .3,
      },
    },
  }, 'scene3');

  mesh.material.needsUpdate = true;
  mesh.side = THREE.DoubleSide;
  mesh.name = recycle;

  let offset = 0;

  aion.add(() => {
    if (Math.abs(scroll.lerp.y) < data.recycle.scale[0]) {
      offset = stage.camera.position.y;
    }

    mesh.position.y = -1.9 + ((stage.camera.position.y - offset) * 0.6);
  }, 'd');
};

const sphere = texture => {
  const mesh = stage.add({
    geometry: {
      type: 'SphereBufferGeometry',
      params: {
        radius: 1,
        widthSegments: 64,
        heightSegments: 64,
      }
    },
    material: {
      type: 'MeshPhongMaterial',
      params: {
        map: texture,
        emissive: stage.colors.violet,
        shininess: 1.,
      }
    },
    mesh: {
      position: {
        x:  window.innerWidth > 768 ? 0.2 : 0.11,
        y: -1.9,
        z: -0.15
      },
      scale: {
        x: window.innerWidth > 768 ? 0.14: 0.1,
        y: window.innerWidth > 768 ? 0.14: 0.1,
        z: window.innerWidth > 768 ? 0.14: 0.1,
      },
      rotation: {
        x: 0,
        y: 0,
        z: THREE.Math.degToRad(15),
      }
    }
  },
    "scene3"
  );

  mesh.name = 'world';

  let time = 0;

  // const tl = anime.timeline({
  //   loop: true,
  //   direction: 'alternate',
  //   autoplay: false,
  // });
  
  // const move = {
  //   x: window.innerWidth > 768 ? 0.4 : 0.15,
  //   y: 0,
  // };

  // tl
  //   .add({
  //     targets: move,
  //     x: (window.innerWidth > 768 ? 1 : 0.1) * -.1,
  //     y: (window.innerWidth > 768 ? 1 : 1) * -.3,
  //     easing: 'linear',
  //     duration: window.innerWidth > 768 ? 2000 : 1200,
  //   })
  //   .add({
  //     targets: move,
  //     x: (window.innerWidth > 768 ? 1 : 0.1) * -.2,
  //     y: (window.innerWidth > 768 ? 1 : 0.4) * .2,
  //     easing: 'linear',
  //     duration: window.innerWidth > 768 ? 2000 : 1200,
  //   })
  //   .add({
  //     targets: move,
  //     x: (window.innerWidth > 768 ? 1 : 0.1) * .3,
  //     y: (window.innerWidth > 768 ? 1 : 1.2) * -.2,
  //     easing: 'linear',
  //     duration: window.innerWidth > 768 ? 2000 : 1200,
  //   })
  //   .add({
  //     targets: move,
  //     x: (window.innerWidth > 768 ? 1 : 0.02) * -.3,
  //     y: (window.innerWidth > 768 ? 1 : 0.8) * 0,
  //     easing: 'linear',
  //     duration: window.innerWidth > 768 ? 2000 : 1200,
  //   })
  //   .add({
  //     targets: move,
  //     x: (window.innerWidth > 768 ? 1 : 0.05) * .1,
  //     y: (window.innerWidth > 768 ? 1 : 0.6) * .2,
  //     easing: 'linear',
  //     duration: window.innerWidth > 768 ? 2000 : 1200,
  //   });

  let offset = 0;
  // const init = window.innerWidth > 768 ? -3.35 : -3.2;
  // const init = window.innerWidth > 768 ? -1.88: -2;
  const init = window.innerWidth > 768 ? -3.4: -3.4;

  aion.add(() => {
    time += 0.01;
    mesh.rotation.y = Math.sin(time * 0.1) * 360 * THREE.Math.DEG2RAD;

    if (Math.abs(scroll.lerp.y) < data.contact.min) {
      // if (!tl.paused) tl.pause();
      offset = stage.camera.position.y;
    } else {
      // if (tl.paused) tl.play();
    }
    mesh.position.y = init + ((stage.camera.position.y - offset) * 0.7); // move.y;
    // mesh.position.y = init + move.y;
    // mesh.position.x = move.x;
  }, 'c');
};

Object.assign(scene, {
  product,
  recycle,
  sphere,
});
